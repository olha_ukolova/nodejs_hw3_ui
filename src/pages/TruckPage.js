import { Header } from "../components/header";
import { Trucks } from "../components/Trucks";

export const TruckPage = () => {
  return (
    <>
      <Header person="DRIVER" />
      <div className="form-wrapper text-center">
        <Trucks />
      </div>
    </>
  );
};
