import { useState } from "react";
import { useHistory } from "react-router";
import * as ROUTES from "../constants/routes";
import axios from "axios";
import { HOST } from "../config";

export const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  let login = (e) => {
    e.preventDefault();
    if (!email || !password) {
      return false;
    } else {
      const userObj = {
        email: email,
        password: password,
      };
      let data = JSON.stringify(userObj);
      axios({
        method: "post",
        url: `${HOST}/auth/login/`,
        data: data,
        headers: { "Content-Type": "application/json; charset=utf-8" },
      })
        .then(function (res) {
          console.log(res);
          alert(res.data.message);
          localStorage.setItem("token", res.data.jwt_token);
          axios({
            method: "get",
            url: `${HOST}/users/me/`,
            headers: {
              Authorization: `${localStorage.getItem("token")}`,
            },
          }).then(function (res) {
            console.log(res);
            localStorage.setItem("role", res.data.user.role);
            window.location.href = "/";
          });
        })

        .catch(function (error) {
          console.log(error);
          alert("Error login or password");
        });

      setEmail("");
      setPassword("");
      console.log("ok");
    }
  };

  return (
    <div className="form-wrapper px-5 m-5 border border-secondary text-center">
      <form className="px-5" onSubmit={(e) => login(e)}>
        <div className="mb-3">
          <h1>Login</h1>
          <input
            type="text"
            className="form-control mb-3"
            placeholder="Email address (email@email.com or email@email.net)"
            pattern="^([\w\.\-]+)@([\w\-]+)(\.net||\.com)$"
            value={email}
            onChange={({ target }) => setEmail(target.value)}
          />
          <input
            type="password"
            className="form-control mb-3"
            value={password}
            autoComplete="off"
            placeholder="Password"
            pattern="[\w\s]{2,}"
            onChange={({ target }) => setPassword(target.value)}
          />
          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </div>
      </form>
      <div className="mb-3">
        <h3>If you aren't register press button </h3>
        <button
          type="submit"
          className="btn btn-danger"
          disabled={false}
          onClick={(e) => {
            e.preventDefault();
            history.push(ROUTES.REGISTER);
          }}
        >
          Register
        </button>
      </div>
    </div>
  );
};
