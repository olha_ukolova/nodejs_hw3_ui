import { useEffect, useState } from "react";
import { Header } from "../components/header";
import axios from "axios";
import { HOST } from "../config";

export const Profile = () => {
  const [password, setPassword] = useState("");
  const [newPassword, setNEWPassword] = useState("");
  let [edit, setEdit] = useState(false);
  let [profile, setProfile] = useState(false);

  useEffect(() => {
    async function fetchData() {
      console.log("mau");
      await axios({
        method: "get",
        url: `${HOST}/users/me`,
        headers: {
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          setProfile(res.data);
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
          localStorage.removeItem("token");
          localStorage.removeItem("role");
        });
    }
    fetchData();
  }, [edit]);

  let submit = (e) => {
    e.preventDefault();
    if (!password || !newPassword) {
      return false;
    } else {
      const userObj = {
        oldPassword: password,
        newPassword: newPassword,
      };

      let data = JSON.stringify(userObj);
      axios({
        method: "patch",
        url: `${HOST}/users/me/password`,
        data: data,
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          alert(res.data.message);
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
          alert("Try again");
        });
      console.log("ok");
    }
  };

  const deleteProfile = () => {
    axios({
      method: "delete",
      url: `${HOST}/users/me`,
      headers: {
        Authorization: `${localStorage.getItem("token")}`,
      },
    })
      .then(function (res) {
        console.log(res);
        alert(res.data.message);
        localStorage.removeItem("token");
        localStorage.removeItem("role");
        window.location.href = "/";
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const logOut = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    window.location.href = "/";
  };

  return (
    <div className="form-wrapper  text-center">
      <Header person={localStorage.getItem("role")} />
      {profile && profile.user ? (
        edit ? (
          <div className="form-wrapper text-center">
            <form className="px-5" onSubmit={(e) => submit(e)}>
              <div className="mb-3">
                <h1>Change password</h1>
                <input
                  type="password"
                  className="form-control mb-3"
                  value={password}
                  autoComplete="off"
                  placeholder="Old Password"
                  pattern="[\w\s]{2,}"
                  onChange={({ target }) => setPassword(target.value)}
                />
                <input
                  type="password"
                  className="form-control mb-3"
                  value={newPassword}
                  autoComplete="off"
                  placeholder="New Password"
                  pattern="[\w\s]{2,}"
                  onChange={({ target }) => setNEWPassword(target.value)}
                />

                <button type="submit" className="btn btn-primary">
                  Ok
                </button>
              </div>
              <button
                className="btn btn-warning m-3"
                onClick={() => setEdit(false)}
              >
                Back
              </button>
            </form>
          </div>
        ) : (
          <div className="card  text-white bg-dark mx-5 my-3 px-5">
            <h2 className="card-header text-center">User {profile.user._id}</h2>
            <div className="card-body">
              <h5 className="card-title fw-normal">
                Role: {profile.user.role}
              </h5>
              <h5 className="card-title fw-normal">
                Email: {profile.user.email}
              </h5>
              <h5 className="card-title fw-normal">
                Created date: {profile.user.created_date}
              </h5>
              <div className="text-center">
                <button
                  className="btn btn-warning m-3"
                  onClick={() => setEdit(true)}
                >
                  Edit Password
                </button>
                <button
                  className="btn btn-danger m-3"
                  onClick={(e) => deleteProfile()}
                >
                  Delete
                </button>
                <button
                  className="btn btn-warning m-3"
                  onClick={(e) => logOut()}
                >
                  Log out
                </button>
              </div>
            </div>
          </div>
        )
      ) : (
        <h2>Nothing found</h2>
      )}
    </div>
  );
};
