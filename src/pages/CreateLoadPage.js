import { useState } from "react";
import { Header } from "../components/header";
import axios from "axios";
import { HOST } from "../config";

export const CreateLoadPage = () => {
  const [name, setName] = useState("");
  const [payload, setPayload] = useState("");
  const [pickup, setPickup] = useState("");
  const [delivery, setDelivery] = useState("");
  const [width, setWidth] = useState("");
  const [length, setLength] = useState("");
  const [height, setHeight] = useState("");

  let submit = (e) => {
    e.preventDefault();
    if (
      !name ||
      !payload ||
      !pickup ||
      !delivery ||
      !width ||
      !length ||
      !height
    ) {
      return false;
    } else {
      const userObj = {
        name: name,
        payload: payload,
        pickup_address: pickup,
        delivery_address: delivery,
        dimensions: {
          width: width,
          length: length,
          height: height,
        },
      };
      let data = JSON.stringify(userObj);
      axios({
        method: "post",
        url: `${HOST}/loads/`,
        data: data,
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          alert(res.data.message);
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
          alert("Try again");
        });

      setName("");
      setPayload("");
      setPickup("");
      setDelivery("");
      setWidth("");
      setLength("");
      setHeight("");
      console.log("ok");
    }
  };

  return (
    <div className="form-wrapper text-center">
      <Header person="SHIPPER" />
      <form className="px-5" onSubmit={(e) => submit(e)}>
        <div className="mb-3">
          <h1>Create load</h1>
          <input
            type="text"
            className="form-control mb-3"
            placeholder="Name"
            value={name}
            onChange={({ target }) => setName(target.value)}
          />
          <input
            type="number"
            className="form-control mb-3"
            placeholder="Payload"
            value={payload}
            onChange={({ target }) => setPayload(target.value)}
          />
          <input
            type="text"
            className="form-control mb-3"
            placeholder="Pickup address"
            value={pickup}
            onChange={({ target }) => setPickup(target.value)}
          />
          <input
            type="text"
            className="form-control mb-3"
            placeholder="Delivery address"
            value={delivery}
            onChange={({ target }) => setDelivery(target.value)}
          />
          <h2>Sizes:</h2>
          <input
            type="number"
            className="form-control mb-3"
            placeholder="Width"
            value={width}
            onChange={({ target }) => setWidth(target.value)}
          />
          <input
            type="number"
            className="form-control mb-3"
            placeholder="Length"
            value={length}
            onChange={({ target }) => setLength(target.value)}
          />
          <input
            type="number"
            className="form-control mb-3"
            placeholder="Height"
            value={height}
            onChange={({ target }) => setHeight(target.value)}
          />

          <button type="submit" className="btn btn-primary">
            Ok
          </button>
        </div>
      </form>
    </div>
  );
};
