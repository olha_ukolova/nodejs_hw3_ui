import { useState } from "react";
import { useHistory } from "react-router";
import * as ROUTES from "../constants/routes";
import axios from "axios";
import { HOST } from "../config";

export const RegisterPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("DRIVER");

  const history = useHistory();

  let register = (e) => {
    e.preventDefault();
    if (!email || !password || !role) {
      return false;
    } else {
      const userObj = {
        email: email,
        password: password,
        role: role,
      };
      let data = JSON.stringify(userObj);
      axios({
        method: "post",
        url: `${HOST}/auth/register/`,
        data: data,
        headers: { "Content-Type": "application/json; charset=utf-8" },
      })
        .then(function (res) {
          alert(res.data.message);
          history.push(ROUTES.SIGN_IN);
        })
        .catch(function (error) {
          alert("This email is used");
        });

      setEmail("");
      setPassword("");
      console.log("ok");
    }
  };

  return (
    <div className="form-wrapper px-5 m-5 border border-secondary text-center">
      <form className="px-5" onSubmit={(e) => register(e)}>
        <div className="mb-3">
          <h1>Register</h1>
          <input
            type="text"
            className="form-control mb-3"
            placeholder="Email address (email@email.com or email@email.net)"
            pattern="^([\w\.\-]+)@([\w\-]+)(\.net||\.com)$"
            value={email}
            onChange={({ target }) => setEmail(target.value)}
          />

          <select
            className="form-select mb-3"
            value={role}
            onChange={({ target }) => setRole(target.value)}
          >
            <option value="DRIVER">Driver</option>
            <option value="SHIPPER">Shipper</option>
          </select>

          <input
            type="password"
            className="form-control mb-3"
            value={password}
            autoComplete="off"
            placeholder="Password"
            pattern="[\w\s]{2,}"
            onChange={({ target }) => setPassword(target.value)}
          />
          <button type="submit" className="btn btn-primary">
            Register
          </button>
        </div>
      </form>
      <div className="mb-3">
        <h3>If you are register press button </h3>
        <button
          type="submit"
          className="btn btn-danger"
          onClick={(e) => {
            e.preventDefault();
            history.push(ROUTES.SIGN_IN);
          }}
        >
          Login
        </button>
      </div>
    </div>
  );
};
