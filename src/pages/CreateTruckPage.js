import { useState } from "react";
import { Header } from "../components/header";
import axios from "axios";
import { HOST } from "../config";

export const CreateTruckPage = () => {
  const [type, setType] = useState("");

  let submit = (e) => {
    e.preventDefault();
    if (!type) {
      return false;
    } else {
      const userObj = {
        type: type,
      };
      let data = JSON.stringify(userObj);
      axios({
        method: "post",
        url: `${HOST}/trucks/`,
        data: data,
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          alert(res.data.message);
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
          alert("Try again");
        });

      setType("");
      console.log("ok");
    }
  };

  return (
    <>
      <Header person="DRIVER" />
      <div className="form-wrapper  text-center">
        <form className="px-5" onSubmit={(e) => submit(e)}>
          <div className="mb-3">
            <h1>Create truck</h1>
            <select
              className="form-select mb-3"
              value={type}
              onChange={({ target }) => setType(target.value)}
            >
              <option value="SPRINTER">SPRINTER</option>
              <option value="SMALL STRAIGHT">SMALL STRAIGHT</option>
              <option value="LARGE STRAIGHT">LARGE STRAIGHT</option>
            </select>

            <button type="submit" className="btn btn-primary">
              Ok
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
