import { useEffect, useState } from "react";

import axios from "axios";
import { HOST } from "../config";
import { Header } from "../components/header";
import { useHistory } from "react-router";
import * as ROUTES from "../constants/routes";

export const OneLoadPage = () => {
  let history = useHistory();
  let id = window.location.href.split("/").pop();

  let [loadsData, setLoads] = useState([]);
  let [edit, setEdit] = useState(false);
  let [changed, setChanged] = useState(false);
  let [info, setInfo] = useState([]);

  const [name, setName] = useState("");
  const [payload, setPayload] = useState("");
  const [pickup, setPickup] = useState("");
  const [delivery, setDelivery] = useState("");
  const [width, setWidth] = useState("");
  const [length, setLength] = useState("");
  const [height, setHeight] = useState("");

  useEffect(() => {
    async function fetchData() {
      console.log("mau");
      await axios({
        method: "get",
        url: `${HOST}/loads/${id}`,
        headers: {
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          setLoads(res.data);
          setName(loadsData.load.name);
          setPayload(loadsData.load.payload);
          setPickup(loadsData.load.pickup_address);
          setDelivery(loadsData.load.delivery_address);
          setWidth(loadsData.load.dimensions.width);
          setLength(loadsData.load.dimensions.length);
          setHeight(loadsData.load.dimensions.height);
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    fetchData();

    async function shipInfo() {
      await axios({
        method: "get",
        url: `${HOST}/loads/${id}/shipping_info`,
        headers: {
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          console.log(res);
          setInfo(res.data);
          console.log(info);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    if (localStorage.getItem("role") === "SHIPPER") {
      shipInfo();
    }
  }, [edit, changed]);

  let submit = (e) => {
    e.preventDefault();
    if (
      !name ||
      !payload ||
      !pickup ||
      !delivery ||
      !width ||
      !length ||
      !height
    ) {
      return false;
    } else {
      const userObj = {
        name: name,
        payload: payload,
        pickup_address: pickup,
        delivery_address: delivery,
        dimensions: {
          width: width,
          length: length,
          height: height,
        },
      };
      let data = JSON.stringify(userObj);
      axios({
        method: "put",
        url: `${HOST}/loads/${id}`,
        data: data,
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          alert(res.data.message);
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
          alert("Try again");
        });
      console.log("ok");
    }
  };

  const deleteLoad = () => {
    axios({
      method: "delete",
      url: `${HOST}/loads/${id}`,
      headers: {
        Authorization: `${localStorage.getItem("token")}`,
      },
    })
      .then(function (res) {
        console.log(res);
        alert(res.data.message);
        history.push(ROUTES.LOADS);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const postLoad = () => {
    axios({
      method: "post",
      url: `${HOST}/loads/${id}/post`,
      headers: {
        Authorization: `${localStorage.getItem("token")}`,
      },
    })
      .then(function (res) {
        console.log(res);
        alert(res.data.message);
        history.push(ROUTES.LOADS);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const changeState = () => {
    axios({
      method: "patch",
      url: `${HOST}/loads/active/state`,
      headers: {
        Authorization: `${localStorage.getItem("token")}`,
      },
    })
      .then(function (res) {
        console.log(res);
        alert(res.data.message);
        setChanged(!changed)
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className="form-wrapper  ">
      <Header person={localStorage.getItem("role")} />
      {loadsData && loadsData.load ? (
        edit ? (
          <div className="form-wrapper text-center">
            <form className="px-5" onSubmit={(e) => submit(e)}>
              <div className="mb-3">
                <h1>Edit load</h1>
                <input
                  type="text"
                  className="form-control mb-3"
                  placeholder="Name"
                  value={name}
                  onChange={({ target }) => setName(target.value)}
                />
                <input
                  type="number"
                  className="form-control mb-3"
                  placeholder="Payload"
                  value={payload}
                  onChange={({ target }) => setPayload(target.value)}
                />
                <input
                  type="text"
                  className="form-control mb-3"
                  placeholder="Pickup address"
                  value={pickup}
                  onChange={({ target }) => setPickup(target.value)}
                />
                <input
                  type="text"
                  className="form-control mb-3"
                  placeholder="Delivery address"
                  value={delivery}
                  onChange={({ target }) => setDelivery(target.value)}
                />
                <h2>Sizes:</h2>
                <input
                  type="number"
                  className="form-control mb-3"
                  placeholder="Width"
                  value={width}
                  onChange={({ target }) => setWidth(target.value)}
                />
                <input
                  type="number"
                  className="form-control mb-3"
                  placeholder="Length"
                  value={length}
                  onChange={({ target }) => setLength(target.value)}
                />
                <input
                  type="number"
                  className="form-control mb-3"
                  placeholder="Height"
                  value={height}
                  onChange={({ target }) => setHeight(target.value)}
                />

                <button type="submit" className="btn btn-primary">
                  Ok
                </button>
              </div>
              <button
                className="btn btn-warning m-3"
                onClick={() => setEdit(false)}
              >
                Back
              </button>
            </form>
          </div>
        ) : (
          <div className="card  text-white bg-dark mx-5 my-3 px-5">
            <h2 className="card-header text-center">
              Load {loadsData.load._id}
            </h2>
            <div className="card-body">
              <h5 className="card-title fw-normal">
                Created by: {loadsData.load.created_by}
              </h5>
              <h5 className="card-title fw-normal">
                Assigned to: {loadsData.load.assigned_to}
              </h5>
              <h5 className="card-title fw-normal">
                Status: {loadsData.load.status}
              </h5>
              {loadsData.load.state ? (
                <h5 className="card-title fw-normal">
                  State: {loadsData.load.state}
                </h5>
              ) : null}

              <h5 className="card-title fw-normal">
                Name: {loadsData.load.name}
              </h5>
              <h5 className="card-title fw-normal">
                Payload: {loadsData.load.payload}
              </h5>
              <h5 className="card-title fw-normal">
                Pickup Address: {loadsData.load.pickup_address}
              </h5>
              <h5 className="card-title fw-normal">
                Delivery Address: {loadsData.load.delivery_address}
              </h5>
              <h5 className="card-title">Dimensions:</h5>
              <p>Width: {loadsData.load.dimensions.width}</p>
              <p>Length: {loadsData.load.dimensions.length}</p>
              <p>Height: {loadsData.load.dimensions.height}</p>
              {info &&
              info.truck &&
              localStorage.getItem("role") === "SHIPPER" ? (
                <>
                  <h3 className="card-title">Shipping info:</h3>
                  <p>Truck id: {info.truck._id}</p>
                  <p>Truck created by: {info.truck.created_by}</p>
                  <p>Truck assigned to: {info.truck.assigned_to}</p>
                  <p>Truck type: {info.truck.type}</p>
                  <p>Truck status: {info.truck.status}</p>
                  <p>Truck created date: {info.truck.created_date}</p>
                </>
              ) : null}
              {loadsData.load.status === "NEW" &&
              localStorage.getItem("role") === "SHIPPER" ? (
                <div className="text-center">
                  <button
                    className="btn btn-warning m-3"
                    onClick={() => setEdit(true)}
                  >
                    Edit
                  </button>
                  <button
                    className="btn btn-danger m-3"
                    onClick={(e) => deleteLoad()}
                  >
                    Delete
                  </button>
                  <button
                    className="btn btn-success m-3"
                    onClick={(e) => postLoad()}
                  >
                    Post
                  </button>
                </div>
              ) : localStorage.getItem("role") === "DRIVER" &&
                loadsData.load.status === "ASSIGNED" ? (
                <div className="text-center">
                  <button
                    className="btn btn-success m-3"
                    onClick={(e) => changeState()}
                  >
                    Change state
                  </button>
                </div>
              ) : null}
            </div>
          </div>
        )
      ) : (
        <h2>Nothing found</h2>
      )}
    </div>
  );
};
