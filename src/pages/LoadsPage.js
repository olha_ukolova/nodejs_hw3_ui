import { Header } from "../components/header";
import { Loads } from "../components/Loads";

export const LoadsPage = () => {
  return (
    <div className="form-wrapper text-center">
      <Header person={localStorage.getItem("role")} />
      <Loads />
    </div>
  );
};
