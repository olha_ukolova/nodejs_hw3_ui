import "./App.css";
import { CreateLoadPage } from "./pages/CreateLoadPage";
import { CreateTruckPage } from "./pages/CreateTruckPage";
import { LoginPage } from "./pages/LoginPage";
import { RegisterPage } from "./pages/RegisterPage";
import * as ROUTES from "./constants/routes";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import {
  IsUserRedirect,
  ProtectedRoute,
  ProtectedRouteDriver,
  ProtectedRouteShipper,
} from "./helpers/routes";
import { LoadsPage} from "./pages/LoadsPage";
import { TruckPage } from "./pages/TruckPage";
import { Profile } from "./pages/Profile";
import { OneLoadPage } from "./pages/OneLoad";

function App() {
  let role = localStorage.getItem("role");
  return (
    <Router>
      <Switch>
        <IsUserRedirect
          role={role}
          loggedInPath={ROUTES.PROFILE}
          path={ROUTES.SIGN_IN}
        >
          <LoginPage />
        </IsUserRedirect>

        <IsUserRedirect
          role={role}
          loggedInPath={ROUTES.PROFILE}
          path={ROUTES.REGISTER}
        >
          <RegisterPage />
        </IsUserRedirect>

        <ProtectedRoute exact role={role} path={ROUTES.LOADS}>
          <LoadsPage />
        </ProtectedRoute>


        <ProtectedRoute exact role={role} path={ROUTES.PROFILE}>
          <Profile />
        </ProtectedRoute>

        <ProtectedRoute exact role={role} path={ROUTES.ONELOAD + "/:id"}>
          <OneLoadPage />
        </ProtectedRoute>

        <ProtectedRouteDriver role={role} path={ROUTES.TRUCKS}>
          <TruckPage />
        </ProtectedRouteDriver>

        <ProtectedRouteShipper exact role={role} path={ROUTES.CREATELOAD}>
          <CreateLoadPage />
        </ProtectedRouteShipper>

        <ProtectedRouteDriver exact role={role} path={ROUTES.CREATETRUCK}>
          <CreateTruckPage />
        </ProtectedRouteDriver>
      </Switch>
    </Router>
  );
}

export default App;
