export const PROFILE = "/";
export const LOADS = "/loads";
export const TRUCKS = "/trucks";
export const CREATELOAD = "/createload";
export const ONELOAD = "/load";
export const CREATETRUCK = "/createtruck";
export const REGISTER = "/register";
export const SIGN_IN = "/signin";