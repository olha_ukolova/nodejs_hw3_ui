import React from "react";
import { Route, Redirect } from "react-router-dom";

export function IsUserRedirect({ role, loggedInPath, children, ...rest }) {
  return (
    <Route
      {...rest}
      render={() => {
        if (!role) {
          return children;
        }

        if (role === 'DRIVER') {
          return <Redirect to="/driver" />;
        }

        if (role === 'SHIPPER') {
          return <Redirect to="/" />;
        }

        return null;
      }}
    />
  );
}

export function ProtectedRouteShipper({ role, children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (role === 'SHIPPER') {
          return children;
        }

        if (role === 'DRIVER') {
          return <Redirect to="/" />;
        }

        if (!role) {
          return <Redirect to="/signin" />;
        }

        return null;
      }}
    />
  );
}

export function ProtectedRouteDriver({ role, children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (role === 'DRIVER') {
          return children;
        }

        if (role === 'SHIPPER') {
          return <Redirect to="/" />;
        }

        if (!role) {
          return <Redirect to="/signin" />;
        }

        return null;
      }}
    />
  );
}

export function ProtectedRoute({ role, children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (role) {
          return children;
        }

        if (!role) {
          return <Redirect to="/signin" />;
        }

        return null;
      }}
    />
  );
}

