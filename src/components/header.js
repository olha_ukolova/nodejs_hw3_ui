import { useHistory } from "react-router";
import * as ROUTES from "../constants/routes";

export const Header = ({ person }) => {
  const history = useHistory();
  let menu;
  if (person === "DRIVER") {
    menu = [
      { name: "Profile", route: ROUTES.PROFILE },
      { name: "Trucks", route: ROUTES.TRUCKS },
      { name: "Loads", route: ROUTES.LOADS },
    ];
  } else {
    menu = [
      { name: "Profile", route: ROUTES.PROFILE },
      { name: "Loads", route: ROUTES.LOADS },
    ];
  }
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
      <a className="navbar-brand px-3" href="/">
        CrazyDelivery
      </a>
      <button
        className="navbar-toggler mx-3"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
        onClick={() => history.push(ROUTES.PROFILE)}
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse " id="navbarNav">
        <ul className="navbar-nav">
          {menu.map((el) => (
            <li key={el.name} className="nav-item active px-3">
              <a className="nav-link" href={el.route}>
                <span className="sr-only">{el.name}</span>
              </a>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
};
