import { useEffect, useState } from "react";
import * as ROUTES from "../constants/routes";
import axios from "axios";
import { HOST } from "../config";

export const Trucks = () => {
  let [trucksData, setTrucks] = useState([]);
  let [deleted, setDeleted] = useState(true);
  let [edit, setEdit] = useState(false);
  let [editedTruckId, setEditedTr] = useState(false);
  let [assign, setAssign] = useState(false);

  const [type, setType] = useState("");
  useEffect(() => {
    async function fetchData() {
      axios({
        method: "get",
        url: `${HOST}/trucks/`,
        headers: {
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          setTrucks(res.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    fetchData();
  }, [deleted, edit]);

  const deleteTruck = (id) => {
    axios({
      method: "delete",
      url: `${HOST}/trucks/${id}`,
      headers: {
        Authorization: `${localStorage.getItem("token")}`,
      },
    })
      .then(function (res) {
        setDeleted(!deleted);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  let submit = (e) => {
    e.preventDefault();
    if (!type) {
      return false;
    } else {
      const userObj = {
        type: type,
      };
      let data = JSON.stringify(userObj);
      axios({
        method: "put",
        url: `${HOST}/trucks/${editedTruckId}`,
        data: data,
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          alert(res.data.message);
          console.log(res);
        })
        .catch(function (error) {
          console.log(error);
          alert("Try again");
        });

      setType("");
      console.log("ok");
    }
  };

  const assignedTruck = (id) => {
    axios({
      method: "post",
      url: `${HOST}/trucks/${id}/assign`,
      headers: {
        Authorization: `${localStorage.getItem("token")}`,
      },
    })
      .then(function (res) {
        setAssign(!assign);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div>
      <a type="button" href={ROUTES.CREATETRUCK} className="btn btn-dark m-5">
        Add new Truck
      </a>

      {trucksData && trucksData.trucks ? (
        edit ? (
          <div className="form-wrapper text-center">
            <form className="px-5" onSubmit={(e) => submit(e)}>
              <div className="mb-3">
                <h1>Edit truck</h1>
                <select
                  className="form-select mb-3"
                  value={type}
                  onChange={({ target }) => setType(target.value)}
                >
                  <option value="SPRINTER">SPRINTER</option>
                  <option value="SMALL STRAIGHT">SMALL STRAIGHT</option>
                  <option value="LARGE STRAIGHT">LARGE STRAIGHT</option>
                </select>

                <button type="submit" className="btn btn-primary">
                  Ok
                </button>
              </div>
              <button
                className="btn btn-warning m-3"
                onClick={() => setEdit(false)}
              >
                Back
              </button>
            </form>
          </div>
        ) : (
          <table
            className="table table-dark table-striped m-auto p-5 overflow-auto"
            style={{ width: "95%" }}
          >
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Created By</th>
                <th scope="col">Assigned To</th>
                <th scope="col">Type</th>
                <th scope="col">Status</th>
                <th scope="col">Created</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
                <th scope="col">Assign</th>
              </tr>
            </thead>
            <tbody>
              {trucksData.trucks.map((el) => (
                <tr key={el._id}>
                  <th scope="row">{el._id}</th>
                  <td>{el.created_by}</td>
                  <td>{el.assigned_to}</td>
                  <td>{el.type}</td>
                  <td>{el.status}</td>
                  <td>{el.created_date}</td>

                  {el.assigned_to === "nobody" ? (
                    <>
                      <td>
                        <button
                          className="text-decoration-none"
                          onClick={() => {
                            setEditedTr(el._id);
                            setEdit(true);
                          }}
                        >
                          &#128736;
                        </button>
                      </td>
                      <td>
                        <button
                          className="text-decoration-none"
                          onClick={() => deleteTruck(el._id)}
                        >
                          &#10060;
                        </button>
                      </td>
                      <td>
                        <button
                          className="text-decoration-none"
                          onClick={() => assignedTruck(el._id)}
                        >
                          &#9989;
                        </button>
                      </td>
                    </>
                  ) : (
                    <>
                      <td>
                        <p></p>
                      </td>
                      <td>
                        <p></p>
                      </td>
                      <td>
                        <p></p>
                      </td>
                    </>
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        )
      ) : (
        <p>No load</p>
      )}
    </div>
  );
};
