import { useEffect, useState } from "react";
import * as ROUTES from "../constants/routes";
import axios from "axios";
import { HOST } from "../config";

export const Loads = () => {
  let [loadsData, setLoads] = useState([]);
  let [deleted, setDeleted] = useState(true);
  let [active, setActive] = useState(null);
  useEffect(() => {
    async function fetchData() {
      axios({
        method: "get",
        url: `${HOST}/loads/`,
        headers: {
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          setLoads(res.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    fetchData();

    async function activeFind() {
      axios({
        method: "get",
        url: `${HOST}/loads/active`,
        headers: {
          Authorization: `${localStorage.getItem("token")}`,
        },
      })
        .then(function (res) {
          setActive(res.data);
          console.log(active);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    if (localStorage.getItem("role") === "DRIVER") {
      activeFind();
    }
  }, [deleted, active]);

  const deleteLoad = (id) => {
    axios({
      method: "delete",
      url: `${HOST}/loads/${id}`,
      headers: {
        Authorization: `${localStorage.getItem("token")}`,
      },
    })
      .then(function (res) {
        setDeleted(!deleted);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div>
      {localStorage.getItem("role") === "SHIPPER" ? (
        <a type="button" href={ROUTES.CREATELOAD} className="btn btn-dark m-5">
          Add new Load
        </a>
      ) : active ? (
        <a
          type="button"
          href={`${ROUTES.ONELOAD}/${active.load._id}`}
          className="btn btn-dark m-5"
        >
          See active
        </a>
      ) : null}

      {loadsData && loadsData.loads && loadsData.loads.length ? (
        <table
          className="table table-dark table-striped m-auto mt-3 p-5 overflow-auto"
          style={{ width: "95%" }}
        >
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Status</th>
              <th scope="col">Pickup address</th>
              <th scope="col">Delivery address</th>
              <th scope="col">Created</th>
              <th scope="col">Info</th>

              {localStorage.getItem("role") === "SHIPPER" ? (
                <th scope="col">Delete</th>
              ) : (
                <td>
                  <p></p>
                </td>
              )}
            </tr>
          </thead>
          <tbody>
            {loadsData.loads.map((el) => (
              <tr key={el._id}>
                <th scope="row">{el.name}</th>
                <td>{el.status}</td>
                <td>{el.pickup_address}</td>
                <td>{el.delivery_address}</td>
                <td>{el.created_date}</td>
                <td>
                  <a
                    className="text-decoration-none"
                    href={`${ROUTES.ONELOAD}/${el._id}`}
                  >
                    &#128270;
                  </a>
                </td>
                {el.status === "NEW" &&
                localStorage.getItem("role") === "SHIPPER" ? (
                  <td>
                    <button
                      className="text-decoration-none"
                      onClick={() => deleteLoad(el._id)}
                    >
                      &#10060;
                    </button>
                  </td>
                ) : (
                  <td>
                    <p></p>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <p>No load</p>
      )}
    </div>
  );
};
